# Purchase Code

Purchase code is a tool used to generate unique code for iot devices.

## Installation

Run the script to create a docker image. make sure you have installed the docker application. [click here](https://docs.docker.com/engine/install/) for installation

```bash
docker build --tag purchase-code:0.0.1 .
```

Run the script to create a container from a docker image

```bash
docker run -p 9090:8080 -d --name purchase-code-gen purchase-code:0.0.1
```

## Usage

URL: http://localhost:9090/data

request method : POST

```json
{
  "BarisCode": [41, 42, 43, 41, 42, 43]
}
```

response

```json
{
  "arr": "5c5d5e5c5d5e987a"
}
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.
